#Trial codes for finding the peaks in the grpah#

#code for the peak finder#
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from scipy.signal import argrelmax

#code to plot the data onto a graph

df = pd.read_excel("xps-data/xps-data.xls", skiprows = 1)

print(df.head())
df.plot()

#Now writing the code to find the peaks

#got noisy to print the peaks of one column, now we just need to make it do it for 
# each one and set parameters
y_noisy = np.array(df[df.columns[1, 3, 5]].values.tolist())
#write a for loop that goes to all of the columns for data
pkind = np.array(argrelmax(y_noisy,order=1))

x_noisy = np.array(df[df.columns[0, 2, 4]].values.tolist())
pkind = np.array(argrelmax(x_noisy, order=1))

ax = plt.figure().gca()
ax.plot(pkind, noisy[pkind], linestyle='none', marker='*', markersize=10, color=
		'r')
ax.set_xlabel('Binding Energy')
ax.set_ylabel('Intensity')

plt.show()
