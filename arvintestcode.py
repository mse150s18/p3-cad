#Test code Arvin
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
### the modules needed to run the following code

### Here the code is importing an excel file then reading the data in that file
peak_data = pd.read_excel('xps-data/xps-data.xls')
time_series = peak_data['Problem 1a Data']
time_series = time_series.tolist()
data = pd.read_excel('xps-data/xps-data.xls')
df = peak_data[0:15]

### In this portion of the code, the module pandas is processing the code. Then identifing which row to skip as well as attaching labels to the plot function that is asked for. Then returning the data in the form of a graph.

peak_data = pd.read_excel('xps-data/xps-data.xls',skiprows=1)
print(peak_data.head())
peak_data.plot(x='Binding energy (eV)',y='Intensity (counts/second)')
plt.show()
print(peak_data[peak_data.columns[1]])

### Creating defintions for the data and detected peaks

def gen_data(data,x,y,title):
    data.plot(x=x,y=y,title=title, Legend=False)
    plt.xlabel('Binding energy (eV)')
    plt.ylabel('Intensity (counts/second)')
    directory = 'plots'
def detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',
                 kpsh=False, valley=False, show=False, ax=None):
    x = np.atleast_1d(x).astype('float64')
    if x.size < 3:
        return np.array([], dtype=int)
    if valley:
        x = -x
### The code assigns indices to the x value range
    dx = x[1:] - x[:-1]
### Here the code accounts for any possible missing values(nan), those missing values then do not affect things like sum or counts. 
    indnan = np.where(np.isnan(x))[0]
    if indnan.size:
        x[indnan] = np.inf
        dx[np.where(np.isnan(dx))[0]] = np.inf
    ine, ire, ife = np.array([[], [], []], dtype=int)
    if not edge:
        ine = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) > 0))[0]
    else:
        if edge.lower() in ['rising', 'both']:
            ire = np.where((np.hstack((dx, 0)) <= 0) & (np.hstack((0, dx)) > 0))[0]
        if edge.lower() in ['falling', 'both']:
            ife = np.where((np.hstack((dx, 0)) < 0) & (np.hstack((0, dx)) >= 0))[0]
    ind = np.unique(np.hstack((ine, ire, ife)))
###This is the start of the set up the size of any missing values
    if ind.size and indnan.size:
### If any of the (nan) are close to any of the peaks those values cannot be peaks 
        ind = ind[np.in1d(ind, np.unique(np.hstack((indnan, indnan-1, indnan+1))), invert=True)]
###This establishes that the first and last values of the data values cannot be peaks
    if ind.size and ind[0] == 0:
        ind = ind[1:]
    if ind.size and ind[-1] == x.size-1:
        ind = ind[:-1]
###Here the code removes the peaks of minimum height 
    if ind.size and mph is not None:
        ind = ind[x[ind] >= mph]
###Here the removal of any peaks with conditions may conflict with the threshhold parameters size or 0
    if ind.size and threshold > 0:
        dx = np.min(np.vstack([x[ind]-x[ind-1], x[ind]-x[ind+1]]), axis=0)
        ind = np.delete(ind, np.where(dx < threshold)[0])
###Here the code is making corrections for small peaks and the minimum peak distance
    if ind.size and mpd > 1:
        ind = ind[np.argsort(x[ind])][::-1]  
        idel = np.zeros(ind.size, dtype=bool)
        for i in range(ind.size):
            if not idel[i]:
###In line 70 the code sorts by peak height of the values in index,(ind)

###This allows to keep peaks that have the same height if the value of kpsh is true
                idel = idel | (ind >= ind[i] - mpd) & (ind <= ind[i] + mpd) \
                    & (x[ind[i]] > x[ind] if kpsh else True)
                idel[i] = 0  
###This allows to keep current peak and remove smaller peaks and sort through their indices by when they happened in the data
    
        ind = np.sort(ind[~idel])

    if show:
        if indnan.size:
            x[indnan] = np.nan
        if valley:
            x = -x
        _plot(x, mph, mpd, threshold, edge, valley, ax, ind)

    return ind

x = peak_data[peak_data.columns[1]]
detected_peak = detect_peaks(x, mph=None, mpd=1, threshold=0, edge='rising',     kpsh=False,valley=False, show=False, ax=None)
print(detected_peak)

### Here the coder has run out of code, but this carpentry thing works I    believe this makes the person go back and understand what is taking place, sometimes line by line
