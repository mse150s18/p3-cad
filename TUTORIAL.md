# This will be where each of us write 10 lines describing the code we make for the project. Since there are 5 of us we need at least 50 lines of code to explain. Eric clarified that these are 50 unique lines of code.

#Catherine's Code Explanations
1. import numpy
	
	This imports numpy, which allows us to use a certain range of functions mainly
	dealing with numbers and plotting.

2. import pandas as pd

	This imports pandas into the python working place. Pandas is used to process
	data from things like Excel files. Importing it as pd is useful because when writing the
	code one just types pd instead of pandas.

3. import matplotlib.pyplot as plt

	This line of code imports a library of functions that can take data and plot it. Importing
	it as plt is helpful because when writing the code one just writes plt instead of the
	full thing.

4. thing = pd.read_excel(open('xps-data.xls', 'rb'), skiprows = 2)
5. print(thing)

	This line takes the data in the excel file and creates an array that I named 'thing'.
	The skiprows part tells the array to ignore the first two rows which are
	titles in the excel file. This way python is not confused by English words.
	Thing is the name I gave the function (previously it was pleasework) so that I 
	could tell python to print it and therefore see the array.

6. thing.columns = ['x1', 'y1', 'x2', 'y2', 'x3', 'y3']
	This line of code labels the 6 columns in the array so that they can be plotted.
	The columns are simply labeled by x_ and y_ so that when I plot the data I know
	which data corresponds to which dataset.

7. plt.scatter(thing['x1'], thing['y1'])

	This line plots the data from the array, taken from the excel file  as a point 
	scatter plot. The thing['x1'], thing['y1'] call the first two columns of the data, 
	which correspond to question 1A. For the other plots this line was simply copied 
	and 'x1' and 'y1' were replaced by 'x2', 'y2' and 'x3' and 'y3' respectively.

8. plt.title("Problem 1A Data")

	This line of code labels the plot from the previous line of code. The plot used in
	the example above is 'x1' and 'y1' which corresponds to 1A, so I simply named the 
	plot 'Problem 1A' to better keep track of it. The subsequent plots were labeled 
	'Problem 1B' and 'Problem 2'. 

9. plt.show()

	This line creates an image of the plot. Because it is written at the end of the 
	set of lines that plot the two columns show() does not need a name, it just shows
	what is given to it previously. This is repeated at the end of each set of plotting
	code so each plot is printed so I can see them.

10. import scipy

	This imports a set of tools that I am hoping will be able to identify the peaks in 
	the plots and label them. 










#Melissa's Code Explanations
1. data=numpy.loadtxt('filename')
	Defines the data using numpy to load the text and gets it ready to be analyzed later on     

2. directory= 'grains.png'
	This defines the directory we are looking at and taking data fromto make sure we are looking at the right data

3. a=misc.imread('grains.png',flatten=True)
	a is just a definition of a variable. In this case, the code is from scipy and it will find the correct file and then flatten it. The following lines of code say what exactly will be done to the image.

4. print(a.max(),a.min())
	When these terms are defined, the code will print a max and a minfor a, which is this case is the flattened image

5. plt.imshow(a)
	This applies the settings that were done to the defined 'a' image. 

6.  def randpoint(array):
	 Sets a definition of the type of array that is created with the following conditions

7. x=randint(array.shape[1]) y=randint(array.shape[0])
	These conditions refer to directions on the random point to define them

8. return array[y,x]
	Returns an array over the image with defined y and x points

9. row, column = y,x
	defines row and column values for y and x 

10. row, column= row,column+1 
	Takes those initial y and x values and moves them around the image. This can be repeated with changing the row value as well 









#Hailee's Code Explanations:
1. pd.read_excel("file_path")
	Tells pandas to read the excel file and the data that we have inside of the file. 
2. print(df.head())
	This command prints the first few lines of data that the file contains so that we can
	see what our starting data is. 
3. ax.set_ylabel('Ylabel') or ax.set_xlabel('Xlabel')
	This assigns a name to the x or y axis on your graph that you are printing.
4. noisy = np.array(df[df.columns[1]].values.tolist())
	In this line of code, we are asking python to read the first column of data and add that to the 
	array of noisy.
5. pkind = np.array(argrelmax(noisy, order=1))
	This line of code comes after the code written above. This code takes the array that we defined
	in noisy, and plots the data points on a graph.
6. ax.plot(pkind, noisy[pkind], linestyle='none', marker='*', markersize=10, color='r')
	This line of code sets up the characteristics of the peak finding graph. You can tell that
	the data is defined as noisy[pkind], the marker of peaks is a star, and the color of the star
	is red. This makes it easy for us, and the user, to dictate what/where the peaks are.
7. plt.show()
	This tells python to take all of the data and commands that we have put together onto a graph.
	It will display all of the graph characteristics and the peaks it found.
8. from scipy.signal import argrelmax
	Aregrelmax is imported from scipy, which is what makes graphing the arrays and peak findings
	possible. 
9. import numpy as np
	Numpy is what takes multiple data points and puts them into the graph as an array. I used
	np.array to graph the first column onto the graph as well as finding the peaks. 
10. skiprows=1
	When I initially wrote my excel data code, python read it as the first line was the name of the 
	x and y axis'. When I wrote "skiprows=1" this tells python to skip the first line of code and
	to start on the next line.



# Arvin's Code Explainations:
1. dict()
           Creates a new empty dictionary where you describe the content of a value that you want to attach meaning to


2.   len(some list of objects)
      The returns the number of item in"some list"  example, [0,1,2,3,4,5,6]      and returns 7

3.   range()
      This allows for a seqence of items(numbers) to be returned arrangement or desired output. For instance range(terminal),   range (start, terminal), respectively....... range(3), would return     [0,1,2] for "terminal", then range(4,10), would return [4,5,6,7,8,9,10]    for "start, terminal".

4.   pip install (packages or modules)
      When working in the terminal and you get a error message module       not found, pip install(module) allows the module to run in python/terminal.

5.   if statement
       The if statement in code sets up a conditional execution of the         statement declared

6.  while statement
         This is used to repeat the initial "if_statement" as long as the        condition remains true

7.   for statement
       The is used to iterate thru a list of conditions that may be present    in a string or list 
8.   str()
       This function allows the content of a variable to be represented as    a string

9.   num_some object = len(some object)
       The bit of code is used to return the number of objects in an array,   the person coding must use the print(some object) to get the value of the number of objects contained in the array. 

10.  add = ['a', 'b', 'c']
   add.append('d')
   print(add)    

    This piece of code allows for the amendment of an object into an alreadycreated array. this would be handy if an amount of data was in an array and perhaps more data was needed to complete calculations.

11. def scope_test():
    This is used to define a statement in particular classifacation definition, in this case scope  test, these definitions have to be made before they have any effect. 

12.  def do_nonlocal():
    def do_nonlocal():  Here the code has used the definition stament to binds assignments to names into a scope, that allows for binding names to objects, it does not copy the data only gives recognition to the data

13.  tuple = ('some object1', 'some object2') 
    A tuple is very much like a list in that it can be indexed,nested,and have repetition except a tuple is immutable. The objects in a tuple cannot be changed or mutated





#Jesse's Code Explanations
data = pd.read_excel(filename, skiprows=1)
	This line is reading from the excel file given in filename (the command line argument) and 
	assigning it to a new vaiable called data.  The skiprows argument is telling it to skip the
	first row of the file because it's just header text.

gen_plots(data, 'x1', 'y1', 'Plot1', 0.2)
	This line is using the gen_plots function that I've created to analyze the data and generate a
	graph based on it.  The first argument is the data to use.  The second and third are the data
	columns to use for the x and y data, respectively.  The fourth argument is the title.  The last
	argument is the threshold to use for the peak finding function.  It will be different for each
	plot because they all have noise in different locations. This function is called three times 
	for the three different sets of data in the excel sheet.

maxima = pu.indexes(data[y], thres = thresh)
	This line is finding the peaks of the data using the indexes function of peakutils. I didn't
	install peakutils, but instead I copied the functions I needed (indexes and plot) into a file
	and imported it into the main processing file.  The first argument is the data to analyze. 
	The second is a threshold to use so the peak analyzer will ignore unwanted noise.

pu.plot(data[x], data[y], maxima)
	This line plots the peak data using peakutils plot function.  The peakutils plot is used in
	place of matplotlib because matplotlib doesn't have the necessary parameters to plot the
	peak data.  The first and second arguments are the x and y data to use for the plot.  The 
	third argument is an array of peak points to include on the plot.

plt.xlabel('Binding Energy, eV')
	This line adds an x axis label to the plot.  It takes in a string which is then placed on the
	x axis as a label, in this case, Banding energy (in eV)

plt.suptitle(title)
	This line adds a title to the plot.  It takes in a string argument and adds it to the top of the
	plot.  In this case, the title was passed into the function, so I used that variable for the
	title.

if not os.path.exists(directory):
	This is a conditional to check if directory currently exists.  If it doesn't, the next line 
	will execute.

os.makedirs(directory)
	This creates a directory from the directory variable ('plots'), but will only go through if
	the previous conditional is fulfilled.

plt.savefig(directory + '/' + title)
	This line saves the plot to a file called title (passed into the function) in the directory
	directory, which was defined and created in the previous line.

plt.clf()
	This clears the figure so any additional figures added will not cover up the old ones.
