filename = sys.argv[1]
	This line is accessing the command-line argument entered when the user runs the program.  argv 
	is an array, with the first element (index 0) being the program being run.  This is why I used
	the index 1 (the element after the executable name).  It then assigns this value to filename,
	which I will use to read the excel file.	

data = pd.read_excel(filename, skiprows=1)
	This line is reading from the excel file given in filename (the command line argument) and 
	assigning it to a new vaiable called data.  The skiprows argument is telling it to skip the
	first row of the file because it's just header text.

gen_plots(data, 'x1', 'y1', 'Plot1', 0.2)
	This line is using the gen_plots function that I've created to analyze the data and generate a
	graph based on it.  The first argument is the data to use.  The second and third are the data
	columns to use for the x and y data, respectively.  The fourth argument is the title.  The last
	argument is the threshold to use for the peak finding function.  It will be different for each
	plot because they all have noise in different locations. This function is called three times 
	for the three different sets of data in the excel sheet.

maxima = pu.indexes(data[y], thres = thresh)
	This line is finding the peaks of the data using the indexes function of peakutils. I didn't
	install peakutils, but instead I copied the functions I needed (indexes and plot) into a file
	and imported it into the main processing file.  The first argument is the data to analyze. 
	The second is a threshold to use so the peak analyzer will ignore unwanted noise.

pu.plot(data[x], data[y], maxima)
	This line plots the peak data using peakutils plot function.  The peakutils plot is used in
	place of matplotlib because matplotlib doesn't have the necessary parameters to plot the
	peak data.  The first and second arguments are the x and y data to use for the plot.  The 
	third argument is an array of peak points to include on the plot.

plt.xlabel('Binding Energy, eV')
	This line adds an x axis label to the plot.  It takes in a string which is then placed on the
	x axis as a label, in this case, Banding energy (in eV)

plt.suptitle(title)
	This line adds a title to the plot.  It takes in a string argument and adds it to the top of the
	plot.  In this case, the title was passed into the function, so I used that variable for the
	title.

if not os.path.exists(directory):
	This is a conditional to check if directory currently exists.  If it doesn't, the next line 
	will execute.

os.makedirs(directory)
	This creates a directory from the directory variable ('plots'), but will only go through if
	the previous conditional is fulfilled.

plt.savefig(directory + '/' + title)
	This line saves the plot to a file called title (passed into the function) in the directory
	directory, which was defined and created in the previous line.

plt.clf()
	This clears the figure so any additional figures added will not cover up the old ones.
