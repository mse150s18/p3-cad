import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.signal as sg

thing = pd.read_excel(open('xps-data.xls', 'rb'), skiprows = 2) #this grabs the excel data and puts it into a 5000 row, 6 column array
#print(thing) #this prints the 5000 row, 6 column array

thing.columns = ['x1', 'y1', 'x2', 'y2', 'x3', 'y3']
#this labels the 6 columns in the array so that they can be plotted

xpeak1a=[]
ypeak1a=[]

plt.plot(thing['x1'], thing['y1'])
plt.title("Problem 1A Data")
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (counts/second)")
peaks = sg.find_peaks_cwt(thing['y1'],np.array([1,10]), noise_perc=10)
for element in peaks:
	xpeak1a.append(thing['x1'])
	ypeak1a.append(thing['y1'])
plt.plot(xpeak1a, ypeak1a,'+')
#print('Peaks are: %s' % (peaks))
plt.show()
#plt.savefig('problem 1a')


plt.scatter(thing['x2'], thing['y2'])
plt.title("Problem 1B Data")
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (counts/second)")
plt.show()
#plt.savefig('problem 1b')


plt.scatter(thing['x3'], thing['y3'])
plt.title("Problem 2 Data")
plt.xlabel("Binding Energy (eV)")
plt.ylabel("Intensity (counts/second)")
plt.show()
#plt.savefig('problem 2')


