#following code borrowed from p3-autobroncos/josh_practice_code.py on 4/23/18 11:35 am

import matplotlib.pyplot as plt

import scipy.ndimage
import numpy as np
image=scipy.ndimage.imread('grains.png',flatten=True)
#for row in image:
#	for col in row:
#		print(col)

gw= 0 #sets grain width 'gw' initial value to 0. in pixel values. need ot convert to length.  
p=0.68955 #defines one pixel length in micrometers and sets it as the constant p.
grain_width_list=[]
for j in range(1540):
	for i in range (2072):
		if (image[j,i] >= 95):
			gw+=p
		if (image[j,i] <95):
			grain_width_list.append(gw)

plt.hist(grain_width_list,bins=20)
np.mean(grain_width_list) 

print(grain_width_list)
plt.show()
