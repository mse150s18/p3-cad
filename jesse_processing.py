import glob
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import argrelextrema as extr
import peakutils as pu

def gaus(x, a, x0, sigma):
	return a*exp(-(x-x0)**2/(2*sigma**2))

def gen_plots(data, x, y, title, thresh = 0.3):
	maxima = pu.indexes(data[y], thres = thresh)
	pu.plot(data[x], data[y], maxima)
	plt.xlabel('Binding Energy, eV')
	plt.ylabel('Intensity, counts/sec')
	plt.suptitle(title)
	directory = 'plots'
	if not os.path.exists(directory):
	       os.makedirs(directory)
	plt.savefig(directory + '/' + title)	
	plt.show
	plt.clf()
	return

def main():
	filename = sys.argv[1]
	data = pd.read_excel(filename, skiprows=1)		
	data.columns = ['x1','y1','x2','y2','x3','y3']
	gen_plots(data, 'x1', 'y1', 'Plot1', 0.2)
	gen_plots(data, 'x2', 'y2', 'Plot2', 0.3)
	gen_plots(data, 'x3', 'y3', 'Plot3', 0.55)

main()
