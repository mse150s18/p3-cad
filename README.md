# p3-CAD

Overview:
	There were two options for this project.  We chose the first option which was to analyze data and look for peaks.  There was also something about fitting the data to a curve, but that part was unclear because it said there would be an explanation added later and nothing was ever added. We focused on the peak finding portion of the project. The program reads an excel and then utilizes the peakutils library to find the peaks.  This turned out to be the easiest solution to the problem because this set of functions already existed online for this purpose, and we didn't have to spend too much time figuring out how to write our own peak analysis functions.

How to Use:
	The python file processing.py will process the data and plot it with the peaks included.  To run the file, type: python processing.py xps-data/xps-data.xls		
	The third argument is the filepath of the excel file.  The program takes in a file as a command line argument, so that portion is necessary in order to make sure it's processing the correct file.  Once the program is run, the resultin plots will be placed in the "plots" directory (they will not automatically show up on the screen).  There should be three plots for the three different sets of data.  The program is specific to the given excel file, so it probably won't work on anything else.  However, it is set up so that it could easily be modified to accept other files.
	
Acknowledgements:
	A portion of the peakutils library was used for this project and should get recognition.  This library was not included in the python files.  Instead, it was looked up online, and a portion of the code was copied into the "peakutils.py" file, located in the project directory. Also special thanks to the instructors for their assistance along the way.
